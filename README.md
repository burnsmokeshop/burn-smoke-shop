Established in 2003. Our original store is at Hwy 6 and Bellaire on the Southwest side of Houston, TX. We have been in business since 2003 and we have expanded across Houston to serve you in 4 locations. You can visit our original location on Hwy6, our Montrose location, Our Westchase location or our location conveniently located in Rice Village. All our stores carry a full assortment of smoking accessories and supplies.

Website: https://burnsmokeshophouston.com/
